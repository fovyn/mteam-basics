const fs = require('fs');
const bodyParser = require('body-parser');
const jsonServer = require('json-server');
const jwt = require('jsonwebtoken');

const server = jsonServer.create();
const router = jsonServer.router(`${__dirname}/forum.json`);

server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());
server.use(jsonServer.defaults());

const SECRET_KEY = '123456789';
const expiresIn = '1h';

function createToken(paylaod) {
  return jwt.sign(paylaod, SECRET_KEY, {expiresIn});
}

function verifyToken(token) {
  return jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ? decode : err);
}

function isAuthenticated(db, {username, password}) {
  return db.findIndex(it => it.username === username && it.password === password) !== -1;
}

server.post('/auth/login', (req, res) => {
  const {username, password} = req.body;

  const db = JSON.parse(fs.readFileSync(`${__dirname}/forum.json`, 'UTF-8'));

  if (isAuthenticated(db.users,{username, password}) === false) {
    const status = 401;
    const message = 'Incorrect email or password';
    res.status(status).json({status, message});
    return;
  }

  const user = db.users.find(it => it.username === username);
  const token = createToken({userId: user.id, roles: ['ROLE_USER']});
  res.status(200).json({token});
});

server.use(/^(?!\/auth).*$/, (req, res, next) => {
  const authorization = req.headers.authorization;

  if (authorization === undefined || authorization.split(' ')[0] !== 'Bearer'){
    const status = 401
    const message = 'Bad authorization header'
    res.status(status).json({status, message})
    return
  }
  try {
    verifyToken(authorization.split(' ')[1]);
    console.log("TEST");
    next()
  } catch (err) {
    const status = 401
    const message = 'Error: access_token is not valid'
    res.status(status).json({status, message})
  }
});

server.use(router);

server.listen(3000, _ => console.log('Run Auth api', __dirname));
