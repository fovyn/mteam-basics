import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SubjectComponent} from './subject.component';
import {SubjectListComponent} from './components/subject-list/subject-list.component';
import {SubjectAddComponent} from './components/subject-add/subject-add.component';

const routes: Routes = [
  { path: '', component: SubjectComponent, children: [
      { path: 'list', component: SubjectListComponent },
      { path: 'add', component: SubjectAddComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubjectRoutingModule { }
