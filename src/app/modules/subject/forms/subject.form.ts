import {FormDeclaration} from '../../../types/form.type';
import {FormArray, FormControl, Validators} from '@angular/forms';
import {format} from 'date-fns';

export const F_SubjectAdd: FormDeclaration = {
  "title": new FormControl(null, [Validators.required]),
  "userId": new FormControl(null, [Validators.required, Validators.min(1)]),
  "createAt": new FormControl(format(new Date(), 'yyyy-mm-dd'), [Validators.required]),
  "keyWords": new FormArray([], [Validators.minLength(1)])
}
