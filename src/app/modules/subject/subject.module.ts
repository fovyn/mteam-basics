import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubjectRoutingModule } from './subject-routing.module';
import { SubjectComponent } from './subject.component';
import { SubjectListComponent } from './components/subject-list/subject-list.component';
import { SubjectAddComponent } from './components/subject-add/subject-add.component';
import {SubjectService} from './services/subject.service';
import {ReactiveFormsModule} from '@angular/forms';
import {AppModule} from '../../app.module';
import {MChipsModule} from '../shared/modules/m-chips/m-chips.module';


@NgModule({
  declarations: [
    SubjectComponent,
    SubjectListComponent,
    SubjectAddComponent
  ],
  imports: [
    CommonModule,
    SubjectRoutingModule,
    ReactiveFormsModule,
    MChipsModule
  ],
  providers: [
    SubjectService
  ]
})
export class SubjectModule { }
