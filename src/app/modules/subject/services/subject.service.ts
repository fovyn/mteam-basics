import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {SubjectDTO, SubjectDTOs, Subjects} from '../../../types/subject.type';
import {HttpClient} from '@angular/common/http';
import {Subject, SubjectMapper} from '../../../models/subject.model';
import {map} from 'rxjs/operators';

@Injectable()
export class SubjectService {
  private _subjects$ = new BehaviorSubject<Subjects>([]);
  get subjects$(): Observable<Subjects> { return  this._subjects$.asObservable(); }

  constructor(private $http: HttpClient) { }

  add(subject: Subject): Observable<SubjectDTOs> {

    this.$http
      .post<SubjectDTO>("http://localhost:3000/subjects", SubjectMapper.toDTO(subject))
      .pipe(map(it => SubjectMapper.fromDTO(it)))
      .subscribe(data => this._subjects$.next([...this._subjects$.value, data]));

    return this.subjects$;
  }
}
