import {Component, OnDestroy, OnInit} from '@angular/core';
import {SubjectService} from '../../services/subject.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'subject-list',
  templateUrl: './subject-list.component.html',
  styleUrls: ['./subject-list.component.scss']
})
export class SubjectListComponent implements OnInit, OnDestroy {
  private _subjectsSubscription: Subscription;

  constructor(private $ss: SubjectService) { }

  ngOnInit(): void {
    this._subjectsSubscription = this.$ss.subjects$.subscribe(subjects => console.log(subjects));
  }

  ngOnDestroy() {
    this._subjectsSubscription.unsubscribe();
  }

}
