import {Injectable} from '@angular/core';
import {SubjectService} from '../../services/subject.service';
import {Observable, PartialObserver, Subscription} from 'rxjs';
import {SubjectDTOs} from '../../../../types/subject.type';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {F_SubjectAdd} from '../../forms/subject.form';
import {Subject} from '../../../../models/subject.model';

@Injectable()
export class SubjectAddFacade {
  private _fg: FormGroup;
  get fg(): FormGroup { return this._fg; }
  get formIsValid(): boolean { return this._fg.valid; }
  get faKeywords(): FormArray { return this._fg.get("keyWords") as FormArray; }

  constructor(
    private $ss: SubjectService,
    private $formBuilder: FormBuilder
  ) {
    this._fg = $formBuilder.group(F_SubjectAdd);
  }

  addAction() {
    if (this.formIsValid) {
      this.$ss.add(new Subject(this._fg.value))
    }
  }

  updateKeywords(keywords: string[]) {
    this.faKeywords.clear();
    keywords.forEach(kw => this.faKeywords.push(new FormControl(kw)));
  }
}
