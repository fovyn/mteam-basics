import { Component, OnInit } from '@angular/core';
import {SubjectService} from '../../services/subject.service';
import {Subject} from '../../../../models/subject.model';
import {SubjectAddFacade} from './subject-add.facade';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'subject-add',
  templateUrl: './subject-add.component.html',
  styleUrls: ['./subject-add.component.scss'],
  providers: [SubjectAddFacade]
})
export class SubjectAddComponent implements OnInit {

  get createForm(): FormGroup { return this.$saf.fg; }

  constructor(private $saf: SubjectAddFacade) { }

  ngOnInit(): void {

  }

  handleSubmitAction() {
    this.$saf.addAction();
  }

  updateKeywords($event: { tag: string }[]) {
    this.$saf.updateKeywords($event.map(it => it.tag));
  }
}
