import {AfterViewInit, Directive, ElementRef, Output, EventEmitter, OnDestroy, Input} from '@angular/core';

@Directive({
  selector: '[mChips]'
})
export class MChipsDirective implements AfterViewInit, OnDestroy {
  @Input("default") tags: M.AutocompleteData = {};
  @Input('placeholder') placeholder: string = null
  @Output("update") updateEvent = new EventEmitter<any[]>();

  private _instance: M.Chips;

  get el(): HTMLDivElement { return this.$elRef.nativeElement; }

  constructor(private $elRef: ElementRef<HTMLDivElement>) { }

  ngAfterViewInit(): void {
    this.el.classList.add("chips", "chips-autocomplete");

    this._instance = M.Chips.init(this.el, {
      onChipAdd: () => this.updateEvent.emit(this._instance.chipsData),
      onChipDelete: () => this.updateEvent.emit(this._instance.chipsData),
      autocompleteOptions: {
        data: this.tags
      },
      placeholder: this.placeholder || ""
    });
  }

  ngOnDestroy(): void {
    this._instance.destroy();
  }
}
