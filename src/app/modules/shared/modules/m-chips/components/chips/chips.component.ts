import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'chips',
  templateUrl: './chips.component.html',
  styleUrls: ['./chips.component.scss']
})
export class ChipsComponent implements OnInit {
  @Input('placeholder') placeholder: string = null;
  @Input('default') default: M.AutocompleteData = {};

  @Output('update') updateEvent = new EventEmitter<any[]>();

  data: Array<any> = [];
  get nbChips(): number { return this.data.length; }

  constructor() { }

  ngOnInit(): void {
  }

  handleUpdate(data: any[]) {
    this.updateEvent.emit(data);
    this.data = data;
  }
}
