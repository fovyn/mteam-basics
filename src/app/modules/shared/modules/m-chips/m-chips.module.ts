import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MChipsDirective} from './directives/m-chips.directive';
import { ChipsComponent } from './components/chips/chips.component';



@NgModule({
  declarations: [
    MChipsDirective,
    ChipsComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MChipsDirective,
    ChipsComponent
  ]
})
export class MChipsModule { }
