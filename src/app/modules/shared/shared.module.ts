import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './components/table/table.component';
import {CustomFormModule} from './modules/custom-form/custom-form.module';



@NgModule({
  declarations: [
    TableComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    TableComponent,
    CustomFormModule
  ]
})
export class SharedModule { }
