import {UserDTO} from '../../../types/user.type';

export class User {
  id: number;
  username: string;
  password: string;
  createAt: string;
  updateAt: string;
  isActive: true;

  constructor(obj?: Partial<User>) {
    this.id = obj && obj.id || 0;
    this.username = obj && obj.username || "";
    this.password = obj && obj.password || "";
    this.createAt = obj && obj.createAt || "";
    this.updateAt = obj && obj.updateAt || "";
    this.isActive = obj?.isActive;
  }
}

export class UserMapper {
  static fromDTO(dto: UserDTO): User {
    const {id, username, password, createAt, updateAt} = dto;
    return new User({id, username, password, createAt, updateAt, isActive: true});
  }
}
