import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {UserDTO} from '../../../types/user.type';
import {HttpClient} from '@angular/common/http';
import {User, UserMapper} from '../models/user.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserResolver implements Resolve<UserDTO> {
  private _pattern = /\d+/ig

  constructor(private $http: HttpClient, private $router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    const id = route.paramMap.get("id");
    if (!id?.match(this._pattern)) {
      this.$router.navigate(["/user", 1]).then(_ => console.log("Navigate"));
      return;
    }

    return this.$http.get<UserDTO>("http://localhost:3000/users/"+ id)
      .pipe(map(it => UserMapper.fromDTO(it)));
  }
}
