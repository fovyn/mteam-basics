import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UserListComponent} from './components/user-list/user-list.component';
import {UserDetailComponent} from './components/user-detail/user-detail.component';
import {UserResolver} from './resolvers/user.resolver';

const routes: Routes = [
  { path: 'user', children: [
      { path: 'list', component: UserListComponent },
      { path: ':id', component: UserDetailComponent, resolve: {"user": UserResolver} },
      { path: '', redirectTo: 'list', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
