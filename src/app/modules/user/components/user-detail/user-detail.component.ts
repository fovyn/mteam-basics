import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  constructor(private $activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.$activatedRoute.data.subscribe(data => console.log(data));
  }

}
