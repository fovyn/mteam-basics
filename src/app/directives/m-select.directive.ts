import {AfterViewInit, Directive, ElementRef, Input} from '@angular/core';
import * as M from 'materialize-css';
import {Observable} from 'rxjs';

@Directive({
  selector: '[mSelect]'
})
export class MSelectDirective implements AfterViewInit {
  // Données sur lesquelles travaille la directive
  @Input('mSelect') data$: Observable<any>
  // Récupère la fabrique pour construire un option a partir de mes datas
  @Input("optionFactory") optionFactory: (it: any) => ({label: string, value: any});

  // Instance de FormSelect
  private _mSelect: M.FormSelect;

  // Le HTML SelectElement => le select pure
  get select(): HTMLSelectElement { return this.elRef.nativeElement; }

  constructor(private elRef: ElementRef<HTMLSelectElement>) { }

  ngAfterViewInit() {
    this.data$.subscribe(data => {
      this._createOption("User", 0, true, true);
      data.forEach(it => this.createNewOption(it));

      this._mSelect = M.FormSelect.init(this.select, {});
    });
  }

  private createNewOption(it) {
    console.log("SELECT", this.select);
    const {label, value} = this.optionFactory(it);

    this._createOption(label, value);
  }

  private _createOption(label: string, value: any, selected: boolean = false, disabled: boolean = false) {
    const option = document.createElement("option");
    option.innerText = label;
    option.value = value;
    option.selected = selected;
    option.disabled = disabled;

    this.select.add(option);
  }

  reset() {
    console.log("RESET");
    this._mSelect.el.children[0].setAttribute("selected", "selected");
  }
}
