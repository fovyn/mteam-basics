import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

export const APP_ROUTES: Routes = [
  { path: 'subject', loadChildren: () => import('./modules/subject/subject.module').then(m => m.SubjectModule) }
]

@NgModule({
  imports: [
    RouterModule.forRoot(APP_ROUTES)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
