import {SubjectDTO} from './subject.type';

export type UserDTO = {
  id: number,
  username: string,
  password: string,
  createAt: string,
  updateAt: string,
  subjects?: Array<SubjectDTO>
};
