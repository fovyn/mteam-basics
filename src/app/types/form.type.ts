import {AbstractControl, FormControl} from '@angular/forms';

// key => nom de l'input
// abstractControl => le type d'input
export type FormDeclaration = {[key: string]: AbstractControl};
